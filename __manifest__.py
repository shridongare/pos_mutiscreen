# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Point of Sale Multiscreen',
    'version': '1.0',
    'category': 'Point of Sale',
    'sequence': 6,
    'summary': 'Mutiscreen',
    'description': """


""",
    'depends': ['point_of_sale'],
    'data': [
        'views/pos_templates.xml',
        'views/pos_config_views.xml'
    ],
    'qweb': [
        'static/src/xml/pos.xml',
    ],
    'installable': True,
    'website': 'https://www.odoo.com/page/point-of-sale',
}
